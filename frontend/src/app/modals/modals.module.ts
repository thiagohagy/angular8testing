import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../helpers/auth.guard';
import { ModalsComponent } from './modals.component';

import {
  MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule
} from '@angular/material';


@NgModule({
  declarations: [
    ModalsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule,
    RouterModule.forChild([
      {
        path: 'modals',
        canActivate: [AuthGuard],
        component: ModalsComponent,
      }
    ])
  ],
  exports: [
    MatDialogModule, MatFormFieldModule, MatButtonModule, MatInputModule,
    RouterModule
  ]
})

export class ModalsModule { }
