import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {

  stages =  [
    {
      name: 'lorem 1',
      id: 1,
    },
    {
      name: 'lorem ipsum 2',
      id: 2,
    },
    {
      name: 'lorem ipsum sin 3 ',
      id: 3,
    },
    {
      name: 'lorem ipsum sin 4',
      id: 4,
    },
    {
      name: 'lorem 5',
      id: 5,
    },
    {
      name: 'lorem i6 ',
      id: 6,
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
