import { Component, OnInit ,Input } from '@angular/core';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {

  @Input() stage;

  cards =  [
    {
      name: 'lorem',
      id: 1,
    },
    {
      name: 'lorem ipsum',
      id: 2,
    },
    {
      name: 'lorem ipsum sin ',
      id: 3,
    },
  ];


  constructor() { }

  ngOnInit() {
  }

}
