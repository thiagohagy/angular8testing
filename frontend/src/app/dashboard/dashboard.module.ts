import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../helpers/auth.guard';
import { DashComponent } from './dash/dash.component';
import { StageComponent } from './stage/stage.component';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [
    DashComponent,
    StageComponent,
    CardComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        component: DashComponent,
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})

export class DashboardModule { }
