import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NavComponent } from './shared/nav/nav.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AuthInterceptor } from './helpers/auth.interceptor';
import { AuthGuard } from './helpers/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { LoadingIndicatorComponent } from './shared/loading-indicator/loading-indicator.component';
import { AppComponent } from './app.component';

import { SidebarComponent } from './shared/sidebar/sidebar.component';

import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ModalsModule } from './modals/modals.module';
import { MyModalComponent } from './modals/my-modal/my-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoadingIndicatorComponent,
    SidebarComponent,
    MyModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AuthModule,
    UsersModule,
    DashboardModule,
    ModalsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true},
    AuthGuard,
  ],
  entryComponents: [MyModalComponent],
  bootstrap: [AppComponent],
})
export class AppModule { }
